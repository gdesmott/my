dnf install -y vim-X11 htop zoxide

dnf install -y langpacks-en glibc-all-langpacks

dnf install -y git-core ninja-build dnf-plugins-core python3-pip indent vim abseil-cpp-devel pulseaudio-libs-devel
pip install git-revise

# Add rpm fusion repositories in order to access all of the gst plugins
sudo dnf install -y \
  "https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm" \
  "https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm"

dnf upgrade -y

# install rest of the extra deps
dnf install -y \
    SDL2 \
    SDL2-devel \
    aalib-devel \
    aom \
    bat \
    bison \
    ccache \
    clang-devel \
    cmake \
    elfutils \
    elfutils-devel \
    elfutils-libs \
    faac-devel \
    ffmpeg \
    ffmpeg-libs \
    flex \
    flite \
    flite-devel \
    gcc \
    gcc-c++ \
    gdb \
    git-lfs \
    glslc \
    graphene \
    graphene-devel \
    gsl \
    gsl-devel \
    gssdp \
    gssdp-devel \
    gtest \
    gtest-devel \
    gtk-doc \
    gtk3 \
    gtk3-devel \
    gtk4-devel \
    gupnp \
    gupnp-devel \
    gupnp-igd \
    gupnp-igd-devel \
    intel-mediasdk-devel \
    json-glib \
    json-glib-devel \
    libaom \
    libaom-devel \
    libasan \
    libasan-static \
    libcaca-devel \
    libdav1d \
    libdav1d-devel \
    libnice \
    libnice-devel \
    libsodium-devel \
    libunwind \
    libunwind-devel \
    libva-devel \
    libxml2-devel \
    libxslt-devel \
    libyaml-devel \
    llvm-devel \
    log4c-devel \
    make \
    mesa-dri-drivers \
    mesa-libGL \
    mesa-libGL-devel \
    mesa-libGLES \
    mesa-libGLES-devel \
    mesa-libGLU \
    mesa-libGLU-devel \
    mesa-libOSMesa \
    mesa-libOSMesa-devel \
    mesa-libOpenCL \
    mesa-libOpenCL-devel \
    mesa-libd3d \
    mesa-libd3d-devel \
    mesa-libgbm \
    mesa-libgbm-devel \
    mesa-omx-drivers \
    mesa-vulkan-drivers \
    mono-devel \
    nasm \
    neon \
    neon-devel \
    npm \
    nunit \
    opencv \
    opencv-devel \
    openjpeg2 \
    openjpeg2-devel \
    patch \
    pipewire-devel \
    procps-ng \
    python3 \
    python3-cairo \
    python3-cairo-devel \
    python3-devel \
    python3-gobject \
    python3-libs \
    python3-wheel \
    qt5-qtbase-devel \
    qt5-qtwayland-devel \
    qt5-qtx11extras-devel \
    qt5-qtquickcontrols \
    qt5-qtquickcontrols2 \
    qt5-qtbase-private-devel \
    redhat-rpm-config \
    sbc \
    sbc-devel \
    valgrind \
    vulkan \
    vulkan-devel \
    wayland-protocols-devel \
    wpebackend-fdo-devel \
    x264 \
    x264-devel \
    x264-libs \
    x265-devel \
    xmlstarlet \
    xorg-x11-server-Xvfb \
    csound-devel \
    gobject-introspection-devel \
    libtheora-devel \
    libvpx-devel \
    google-crc32c-devel \
    librsvg2-devel \
    libsrtp-devel \
    libwebp-devel \
    libgudev-devel \
    qrencode-devel \
    srt-devel \
    librtmp-devel \
    webrtc-audio-processing-devel

# Webkit WPE
dnf copr enable -y philn/wpewebkit
dnf install -y \
    wpewebkit \
    wpewebkit-devel

# Extra tools
dnf install -y \
    apitrace \
    apitrace-gui \
    direnv \
    fish \
    graphviz \
    indent \
    perf \
    rr \
    strace \
    sysprof \
    tmux \
    wget \
    eog \
    google-noto-emoji-color-fonts \
    mold \
    psmisc \
    tig \
    open-sans-fonts \
    yajl \
    gnuplot \
    ack \
    python3-matplotlib \
    git-clang-format

# Install common debug symbols
dnf debuginfo-install -y gtk3 \
    glib2 \
    glibc \
    freetype \
    openjpeg \
    gobject-introspection \
    python3 \
    python3-libs \
    python3-gobject \
    libappstream-glib-devel \
    libjpeg-turbo \
    glib-networking \
    libcurl \
    libsoup \
    nasm \
    nss \
    nss-softokn \
    nss-softokn-freebl \
    nss-sysinit \
    nss-util \
    openssl \
    openssl-libs \
    openssl-pkcs11 \
    brotli \
    bzip2-libs \
    gpm-libs \
    harfbuzz \
    harfbuzz-icu \
    json-c \
    json-glib \
    libbabeltrace \
    libffi \
    libsrtp \
    libunwind \
    mpg123-libs \
    neon \
    orc-compiler \
    orc \
    pixman \
    pulseaudio-libs \
    pulseaudio-libs-glib2 \
    wavpack \
    webrtc-audio-processing \
    ffmpeg \
    ffmpeg-libs \
    faad2-libs \
    libavdevice \
    libmpeg2 \
    faac \
    fdk-aac \
    x264 \
    x264-libs \
    x265 \
    x265-libs \
    xz \
    xz-libs \
    zip \
    zlib \
    libunistring-devel

pip3 install meson hotdoc

# Install the dependencies of gstreamer
dnf builddep -y gstreamer1 \
    gstreamer1-plugins-base \
    gstreamer1-plugins-good \
    gstreamer1-plugins-good-extras \
    gstreamer1-plugins-ugly \
    gstreamer1-plugins-ugly-free \
    gstreamer1-plugins-bad-free \
    gstreamer1-plugins-bad-free-extras \
    gstreamer1-plugins-bad-freeworld \
    gstreamer1-libav \
    gstreamer1-rtsp-server  \
    gstreamer1-vaapi \
    python3-gstreamer1 \
    -x meson

# Remove gst-devel packages installed by builddep above
dnf remove -y "gstreamer1*devel"


# Remove svt-hevc until valgrind ignores it
dnf remove -y gstreamer1-svt-hevc 'svt-hevc*'

# Remove Qt5 devel packages as we haven't tested building it and
# it leads to build issues in examples.
dnf remove -y "qt5-qtbase-devel"

# FIXME: Why does installing directly with dnf doesn't actually install
# the documentation files?
dnf download glib2-doc gdk-pixbuf2-devel*x86_64* gtk3-devel-docs
rpm -i --reinstall *.rpm
rm -f *.rpm

# kubernetes
# https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/
cat <<EOF | sudo tee /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-\$basearch
enabled=1
gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
EOF
sudo yum install -y kubectl

# vscode
# https://code.visualstudio.com/docs/setup/linux#_rhel-fedora-and-centos-based-distributions
rpm --import https://packages.microsoft.com/keys/microsoft.asc
sh -c 'echo -e "[code]\nname=Visual Studio Code\nbaseurl=https://packages.microsoft.com/yumrepos/vscode\nenabled=1\ngpgcheck=1\ngpgkey=https://packages.microsoft.com/keys/microsoft.asc" > /etc/yum.repos.d/vscode.repo'
dnf install -y code
